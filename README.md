# Introduction
This demo shows how to create an Oracle Pluggable Databases (PDB) using a REST API (ORDS).

All the components will be deployed in a Kubernetes cluster on the Oracle Cloud:
![architecture.png](architecture/architecture.png)

The initial setup steps are inspired from the oracle db operator: 
https://github.com/doschkinow/oracle-db-operator


# Creation of the docker images
We start by building docker images as described in https://github.com/oracle/docker-images.git
We will use version 19.3.0 for the Oracle database, and version 19.2.0 for ORDS.

### Download required files
```
git clone https://github.com/oracle/docker-images.git
```

Download the Oracle Database binaries from https://www.oracle.com/database/technologies/oracle-database-software-downloads.html and put the .zip file into the `local` folder.

Download the ORDS binaries from http://www.oracle.com/technetwork/developer-tools/rest-data-services/downloads/index.html and put the .zip file ino the `local` folder.

### Build the Database image

Build the Database image using the provided script (the build process will take a bit more than 10 min):
```
cp local/LINUX.X64_193000_db_home.zip docker-images/OracleDatabase/SingleInstance/dockerfiles/19.3.0/
cd docker-images/OracleDatabase/SingleInstance/dockerfiles
./buildDockerImage.sh -e -v 19.3.0
docker image ls
```

### Upload the Database image to the private repository

If not already done, connect to the OCI console and create:
- an authentication token for your user (User Settings)
- a private registry named "ords" (Developer Services >> Registry). 

Note that we used the Zurich region but this can be replaced by another one.

Now we can upload the image to the registry:
```
docker tag oracle/database:19.3.0-ee zrh.ocir.io/orasealps/ords/database:19.3.0-ee
export OCI_USER='orasealps/oracleidentitycloudservice/guillaume.goutaudier@oracle.com'
export AUTH_TOKEN='REPLACE_WITH _YOUR_TOKEN'
docker login -u $OCI_USER -p $AUTH_TOKEN zrh.ocir.io
docker push zrh.ocir.io/orasealps/ords/database:19.3.0-ee
```

Note that the name of the region (zrh), the name of tenant (orasealps), and your username (guillaume.goutaudier@oracle.com) should be replaced with your own settings. 


### Build the ORDS image
Build the Database image using the provided script (the build process will take a bit more than 10 min):
```
cp local/ords-19.2.0.199.1647.zip docker-images/OracleRestDataServices/dockerfiles/
cd docker-images/OracleRestDataServices/dockerfiles/
# replace "oracle/serverjre:8" by "openjdk:8" in the FROM line 
./buildDockerImage.sh
docker image ls
```

### Upload the ORDS image to the private repository

```
docker tag oracle/restdataservices:19.2.0 zrh.ocir.io/orasealps/ords/restdataservices:19.2.0
docker push zrh.ocir.io/orasealps/ords/restdataservices:19.2.0
```


# Deployment of the Oracle Database and ORDS images on kubernetes
First we need to record our docker credentials as a secret:
```
export EMAIL='<YOUR_EMAIL>'
kubectl create secret docker-registry regcred --docker-server=zrh.ocir.io --docker-username=$OCI_USER --docker-password=$AUTH_TOKEN --docker-email=$EMAIL
```

### Oracle Database deployment
After the database image is deployed, a specific user will need to be created for managing the PDBs. This is documented here: https://docs.oracle.com/en/database/oracle/oracle-rest-data-services/19.4/aelig/enabling-ords-database-api.html#GUID-0CFA8981-1A34-4144-92AE-27A5F7E5BBA7

We will create this user in the container startup script: `/opt/oracle/scripts/startup`. So we first create a configmap to hold this script:
```
kubectl create configmap oracle-db-config --from-file=k8s/db-configmap/
```

Edit `k8s/db.yml` and update the image repository with yours. Then you can start the database:
```
kubectl create -f k8s/db.yml
```
The startup will take around 10min.

### ORDS deployment
For ORDS to be able to manage PDBs, the ORDS database API needs to be enabled and a specific admin user needs to be defined. This is documented here: https://docs.oracle.com/en/database/oracle/oracle-rest-data-services/19.4/aelig/enabling-ords-database-api.html#GUID-8730051B-7C03-487B-954A-7D6786B7EC74. We also need to define the credentials of the user who will use ORDS. 

We will perform all these actions thanks to the configmap:
```
kubectl create configmap oracle-db-ords-config --from-file=k8s/ords-configmap/
```

Edit `k8s/ords.yml` and update the image repository with yours. Then you can start the database:
```
kubectl create -f k8s/ords.yml
```

The ORDS image does not provide an option to automatically run post-install scripts, so we need to run it manually:
```
export ORDS_POD_NAME=$(kubectl get pod -o custom-columns=":metadata.name" | grep oracle-db-ords)
kubectl exec -it $ORDS_POD_NAME -- sh -c /opt/oracle/ords/config/ords/post-install.sh
kubectl delete pod $ORDS_POD_NAME
```


# Playing with ORDS

### Setup client container
The ORDS endpoints that are used to manage the PDB lifecycle are documented here:
https://docs.oracle.com/en/database/oracle/oracle-database/19/dbrst/api-pluggable-database-lifecycle-management.html

We will use some of these endpoints from a client container:

```
cd k8s
kubectl create configmap oracle-client-config --from-file=client-configmap/
kubectl create -f client.yml
kubectl exec -it client /bin/bash
```

Let's make a first test and use the ORDS interface to the list all PDBs:
```
curl -u $ORDS_USERNAME:$ORDS_PASSWORD http://$ORACLE_DB_ORDS_SERVICE_SERVICE_HOST:$ORACLE_DB_ORDS_SERVICE_SERVICE_PORT/ords/_/db-api/stable/database/pdbs/ | jq '.'
```

### Create a PDB
We can use this POST to create the PDB (this can take up to 1 minute):
```
curl -i -X POST -u $ORDS_USERNAME:$ORDS_PASSWORD -d @/conf/create_pdb.json -H "Content-Type:application/json" http://$ORACLE_DB_ORDS_SERVICE_SERVICE_HOST:$ORACLE_DB_ORDS_SERVICE_SERVICE_PORT/ords/_/db-api/stable/database/pdbs/
```

Let's check that the PDB has been properly created:
```
curl -u $ORDS_USERNAME:$ORDS_PASSWORD http://$ORACLE_DB_ORDS_SERVICE_SERVICE_HOST:$ORACLE_DB_ORDS_SERVICE_SERVICE_PORT/ords/_/db-api/stable/database/pdbs/ | jq '.'
```

### Use the PDB 
The PDB has been created with an admin user that belongs to a 'PDB_DBA' group. As this group does not have any privileges, we will not use this user. Instead, we will use the `c##dbapi_cdb_admin` global user that was created at DB startup.  

Let's connect to the database from a python application:
```
cd /conf
python3
>>> import cx_Oracle, os, config
>>> host=os.environ['ORACLE_DB_SERVICE_SERVICE_HOST']
>>> dsn=host+':'+config.port+'/'+config.pdb
>>> connection = cx_Oracle.connect(config.username,config.password,dsn,encoding=config.encoding)
>>> cursor = connection.cursor()
>>> cursor.execute('create table my_tab(my_col number)')
>>> cursor.execute('insert into my_tab values (1)')
>>> cursor.execute('select * from my_tab') 
>>> print(cursor.fetchall())
```

### Delete the PDB
We can delete the PDB using this request:
```
curl -i -X DELETE -u $ORDS_USERNAME:$ORDS_PASSWORD -H "Content-Type:application/json" -H "Accept:application/json" http://$ORACLE_DB_ORDS_SERVICE_SERVICE_HOST:$ORACLE_DB_ORDS_SERVICE_SERVICE_PORT/ords/_/db-api/stable/database/pdbs/pdbtest/?action=INCLUDING
```


# Troubleshooting
Show services in database container:
```
export DB_POD_NAME=$(kubectl get pod -o custom-columns=":metadata.name" | grep oracle-db | grep -v oracle-db-ords)
kubectl exec -it $DB_POD_NAME -- /bin/sh -c "lsnrctl service"
```

Connect directly to the Database container and run sqlplus:
```
export DB_POD_NAME=$(kubectl get pod -o custom-columns=":metadata.name" | grep oracle-db | grep -v oracle-db-ords)
kubectl exec -it $DB_POD_NAME -- /bin/sh -c "sqlplus / as sysdba"
show pdbs;

# Delete PDB
alter pluggable database pdbtest close immediate;
drop pluggable database pdbtest including datafiles;

# Connect to the PDB and create a table:
connect c##dbapi_cdb_admin/PDos00###@localhost:1521/pdbtest;
create table my_tab(my_col number);
insert into my_tab values (1);
select * from my_tab;
```

